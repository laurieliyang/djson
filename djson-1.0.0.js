﻿var DJSON = function (link) {
    if (link === undefined) {
        throw new Error("URL Error !!!");
    }
    var DJSONFunc = function (property) {
        var data;
        var hasLoaded = false;
        delete this[property];
        Object.defineProperty(this, property, {
            get: function () {
                if (hasLoaded === false) {
                    $.ajax({
                        url: link,
                        cache: true,
                        dataType: "text",
                        async: false,
                        success: function (value) {
                            this.hasLoaded = true;
                            data = eval("(" + value + ")");
                            DJSON.init(data);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            throw new Error("Ajax Error !!!\n" + this.url + "\n" + errorThrown);
                        }
                    })
                };
                return data;
            },
            set: function (value) {
                data = value;
            }
        });
    };
    DJSONFunc.isDJSON = true;
    return DJSONFunc;
};

DJSON.init = function (object) {
    for (var property in object) {
        if (object.hasOwnProperty(property)) {
            if (typeof object[property] === "function" && !!object[property].isDJSON) {
                object[property].call(object, property);
            } else if (typeof object[property] === "object") {
                DJSON.init(object[property]);
            }
        }
    }
};